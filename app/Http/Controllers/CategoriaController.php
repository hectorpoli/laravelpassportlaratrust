<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('check-permissions');
    }

    public function index()
    {
        $categoria = Categoria::all();
        return response()->json([
            'success' => true,
            'data' => $categoria
        ]);
    }

    public function store(Request $request)
    {
        $request->validate(Categoria::$createRule);

        $categoria = new Categoria();
        $categoria->nombre = $request->nombre;

        if ($categoria->save())
            return response()->json([
                'success' => true,
                'data' => $categoria->toArray()
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Categoria could not be added'
            ], 500);
    }
}
