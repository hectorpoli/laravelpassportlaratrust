<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'PassportController@login')->name('login');
Route::post('register', 'PassportController@register');

Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@details');
    Route::get('products', 'ProductController@index')->name('index-productos');
    Route::post('products', 'ProductController@store')->name('create-productos');
    Route::get('categoria', 'CategoriaController@index')->name('index-categoria');
    Route::post('categoria', 'CategoriaController@store')->name('create-categoria');
});
